<?php  
date_default_timezone_set("America/New_York");

header("Content-Type: text/event-stream\n\n");

$counter = rand(1, 10);
while (1) {
  // Every second, sent a "ping" event.
  
  echo "event: ping\n";
  $curDate = date(DATE_ISO8601);
  echo 'data: {"time": "' . $curDate . '"}';
  echo "\n\n";
  
  /*----------Mensajes con datos únicamente--------------

  echo ": Al usar dos puntos indicamos comentario"
  //Este mensaje contiene un campo de datos con el valor "cualquier texto"
  echo "data: cualquier texto"
  //Este contiene un campo de datos con el valor "otro mensaje\ncon dos lineas"
  //Nota: la nueva linea entra en el valor.
  echo "data: otro mensaje\n"
  echo "data: con dos lineas"
*/

/*  -----------  Eventos nombrados--------------------------

    event: userconnect
    data: {"username": "bobby", "time": "02:33:48"}

  event: usermessage
    data: {"username": "bobby", "time": "02:34:11", "text": " Holip! "}

  event: userdisconnect
    data: {"username": "bobby", "time": "02:34:23"}

  event: usermessage
    data: {"username": "sean", "time": "02:34:36", "text": " Chaito "}
*/

/*-----------Mezclando y emparejando----------

    event: userconnect
    data: {"username": "bobby", "time": "02:33:48"}

    data: "Hola este es un mensaje cualquiera"

    data: "Acompañado de alguna tarea en espeficco"

    event: usermessage
    data: {"username": "bobby", "time": "02:34:11", "text": "Holip  a todos "}

*/

    

  // Send a simple message at random intervals.
  
  $counter--;
  
  if (!$counter) {
    echo 'data: This is a message at time ' . $curDate . "\n\n";
    $counter = rand(1, 10);
  }
  
  ob_flush();
  flush();
  sleep(1);
}
?>